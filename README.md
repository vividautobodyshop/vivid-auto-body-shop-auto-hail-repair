VIVID is a family-owned Auto Body Repair Shop and Collision Repair Center located in McKinney, TX. Our auto body repair services include Hail Damage Repair (Paintless Dent Repair and Conventional), Collision Repair, and Custom Paint Jobs for all domestic and foreign vehicles.

Address: 2421 East University Drive, Building #1, McKinney, TX 75069, USA

Phone: 972-737-3288

Website: https://www.vividautobody.com
